// Referencias del HTML
const lblTicket = document.querySelector('#lblNuevoTicket');
const btnCrear = document.querySelector('button');


const socket = io();



socket.on('connect', () => {
    btnCrear.disabled = false;

});

socket.on('ultimo-ticket', (ticket) => {
    lblTicket.innerText = `Ticket ${ticket}`;
});

socket.on('disconnect', () => {
    btnCrear.disabled = true;
});


btnCrear.addEventListener('click', () => {
    socket.emit('siguiente-ticket', null, (ticket) => {
        console.log(ticket)
        lblTicket.innerText = ticket;
    })

});
